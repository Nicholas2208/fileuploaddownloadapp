Spring Boot Attestation Application

СУБД MySQL. 

Создать БД с поддержкой кириллицы:

CREATE DATABASE fileuploaddownload CHARACTER SET utf8 COLLATE utf8_unicode_ci;

Размер файлов не должен превышать размер MEDIUMBLOB 

Перехват исключений, связанных с превышением размера фалов и вывод соответствующего сообщения 

пользователю пока не реализованы.

Для скачивания файла нажать на линк с названием.

При первом запуске приложения в application.properties:

spring.jpa.hibernate.ddl-auto = create

при последующих запусках:

spring.jpa.hibernate.ddl-auto = none