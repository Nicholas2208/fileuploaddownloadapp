package com.nw.fileuploaddownload;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "files")
public class Document {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(nullable = false, unique = true)
	private String name;
	
	private long size;
	
	@Column(name = "upload_time")
	private Date uploadTime;
	
	// mediumblob
	@Column(length = 16777215)
	private byte[] content;
	
	public Document(long id, String name, long size) {
		this.id = id;
		this.name = name;
		this.size = size;
	}

}
